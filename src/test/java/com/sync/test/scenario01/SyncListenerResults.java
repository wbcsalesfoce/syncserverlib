package com.sync.test.scenario01;

import com.asynchronization.core.DataSync;
import com.asynchronization.core.ServerMessage;
import com.asynchronization.core.SyncListener;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by rramirezb on 19/01/2015.
 */

public class SyncListenerResults implements SyncListener {
    private int dataToSend;
    private int beforeDiscard;
    private int afterDiscard;
    private int syncData;
    private SyncListener otherListener;

    public SyncListenerResults() {

        init();
    }

    public SyncListenerResults(SyncListener otherListener) {
        this.otherListener = otherListener;
        init();
    }

    private void init() {
        if (otherListener == null) {
            otherListener = new SyncListener() {
                @Override
                public void message(UUID id, Date date, String message) {

                }

                @Override
                public void dataToSendSize(UUID id, int size) {

                }

                @Override
                public void startingSendingDataToServer(UUID id) {

                }

                @Override
                public void serverSyncFinalized(UUID id, ServerMessage message) {

                }

                @Override
                public void modifiedDataDiscarded(UUID id, int beforeDiscard, int afterDiscard) {

                }

                @Override
                public void synchronizedData(UUID id, Date sync, List<DataSync> datas) {

                }
            };
        }
    }

    @Override
    public void message(UUID id, Date date, String message) {
        System.out.printf("[%s] [%s] %s\n", id, date, message);
        otherListener.message(id, date, message);
    }

    @Override
    public void dataToSendSize(UUID id, int size) {
        dataToSend = size;
        otherListener.dataToSendSize(id, size);

    }

    @Override
    public void startingSendingDataToServer(UUID id) {

        otherListener.startingSendingDataToServer(id);
    }

    @Override
    public void serverSyncFinalized(UUID id, ServerMessage message) {
        otherListener.serverSyncFinalized(id, message);
    }

    @Override
    public void modifiedDataDiscarded(UUID id, int beforeDiscard, int afterDiscard) {
        this.beforeDiscard = beforeDiscard;
        this.afterDiscard = afterDiscard;

    }

    @Override
    public void synchronizedData(UUID id, Date sync, List<DataSync> datas) {
        this.syncData = datas.size();
        System.out.printf("Sync data: %s\n", datas);
    }

    public int getDataToSend() {
        return dataToSend;
    }

    public int getBeforeDiscard() {
        return beforeDiscard;
    }

    public int getAfterDiscard() {
        return afterDiscard;
    }

    public int getSyncData() {
        return syncData;
    }
}
