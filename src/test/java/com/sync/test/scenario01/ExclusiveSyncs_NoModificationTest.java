package com.sync.test.scenario01;

import com.asynchronization.core.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by rramirezb on 13/01/2015.
 */

/**
 * Test of two exclusive synchronizations without modifications.
 */
public class ExclusiveSyncs_NoModificationTest {

    @Test
    public void test() {
        Persistence_ persistence = new Persistence_();



        Store store = new Store(persistence);
        store.getStoreTool().addLink(new ProductStore());
        SyncTool.addTransform(new ProductSyncTransform());


        DataSyncManager data = new DataSyncManager(store);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        Product p = new Product();
        p.setId(UUID.randomUUID());
        p.setName("Coca.Cola");
        persistence.save(p);
        Date lastSync = data.readLastSynchronizationDate();
        Date lastMod = data.readLastModificationDate();
        System.out.println("lastSync "+lastSync);

        System.out.println("lastMod "+lastMod);
        List<DataSync> noSync = data.readNoSync();

        Assert.assertEquals("One element", 1, noSync.size());

        Main.sync(data, new ServerTest(800), null);

        noSync = data.readNoSync();
        Assert.assertEquals("One element", 0, noSync.size());

        Main.sync(data, new ServerTest(300), null);

        noSync = data.readNoSync();
        Assert.assertEquals("One element", 0, noSync.size());

    }
}
