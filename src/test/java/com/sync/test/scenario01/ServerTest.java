package com.sync.test.scenario01;

import com.asynchronization.core.SendState;
import com.asynchronization.core.Server;
import com.asynchronization.core.ServerMessage;

import java.util.List;
import java.util.UUID;

/**
 * Created by rramirezb on 19/01/2015.
 */
public class ServerTest extends Server {
    long delay;

    public ServerTest(long delay) {
        this.delay = delay;
    }

    @Override
    public ServerMessage sendData(UUID id, List<Object> data) {

        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new ServerMessage(id, true, SendState.DATA_SENT, "Sent", null);
    }
}
