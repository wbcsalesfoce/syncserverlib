package com.sync.test.scenario01;

import com.asynchronization.core.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class Sync_WithInclusiveModificationAfterSendTest {
    @Test
    public void test() {
        final Persistence_ persistence = new Persistence_();


        Store store = new Store(persistence);
        store.getStoreTool().addLink(new ProductStore());


        SyncTool.addTransform(new ProductSyncTransform());
        final DataSyncManager data = new DataSyncManager(store);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Date lastSync = data.readLastSynchronizationDate();
        System.out.println("lastSync "+lastSync);
        Product p = new Product();
        p.setId(UUID.randomUUID());
        p.setName("Coca.Cola");

        final Product p2 = new Product();
        p2.setId(UUID.randomUUID());
        p2.setName("Sprite");

        final Product p3 = new Product();
        p2.setId(UUID.randomUUID());
        p2.setName("fanta");

        persistence.save(p);
        persistence.save(p2);
        persistence.save(p3);

        final Callable<Void>  runn2 = new Callable<Void> () {
            @Override
            public Void call() {

                p2.setName("SPRITE");
                persistence.save(p2);
                System.out.printf("%s modified, %s.\n", p2, p2.getLastMod());
                return null;
            }
        };

        final SyncListenerResults results = new SyncListenerResults(new SyncListener() {
            @Override
            public void message(UUID id, Date date, String message) {

            }

            @Override
            public void dataToSendSize(UUID id, int size) {

            }

            @Override
            public void startingSendingDataToServer(UUID id) {

            }

            @Override
            public void serverSyncFinalized(UUID id, ServerMessage serverMessage) {
                try {
                    runn2.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void modifiedDataDiscarded(UUID id, int beforeDiscard, int afterDiscard) {

            }

            @Override
            public void synchronizedData(UUID id, Date sync, List<DataSync> datas) {

            }
        });
        Callable<Void> runn1 = new Callable<Void> () {
            @Override
            public Void call() {
                Main.sync(data, new ServerTest(1000), results);
                List<DataSync> nosync = data.readNoSync();
                System.out.printf("No sync %s\n", nosync);
                return null;
            }
        };



        FutureTask<Void> task1 = new FutureTask<Void>(runn1);
        //FutureTask<Void> task2 = new FutureTask<Void>(runn2);

        ExecutorService service = Executors.newFixedThreadPool(2);
        try {
            service.execute(task1);
          //  service.execute(task2);
            task1.get();
            //task2.get();

            Assert.assertEquals("F", 3, results.getDataToSend());
            Assert.assertEquals("F", 3, results.getBeforeDiscard());
            Assert.assertEquals("F", 2, results.getAfterDiscard());
            Assert.assertEquals("F", 2, results.getSyncData());

            Assert.assertEquals("F", 1, data.readNoSync().size());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println("End");
    }
}
