package com.sync.test.scenario01;

import com.asynchronization.core.DataSync;
import com.asynchronization.core.DataSyncTransform;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class ProductSyncTransform implements DataSyncTransform<Product> {
    @Override
    public DataSync getDataSync(Product o) {
        return new ProductSync(o);
    }

    @Override
    public boolean attend(Object o) {
        return o instanceof Product;
    }
}
