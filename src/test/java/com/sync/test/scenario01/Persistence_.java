package com.sync.test.scenario01;

import com.asynchronization.core.Persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by rramirezb on 16/01/2015.
 */
public class Persistence_ implements Persistence{
    List<Product> products = new ArrayList<Product>();
    Date lastMod;
    Date lastSync;

    {
        lastMod =lastSync = new Date();
    }

    public void addProducts(Product... products){
        Date date = null;
        for (int i = 0; i < products.length; i++) {
            Product product = products[i];
            product.setLastMod(date = new Date());
            this.products.add(product);
        }
        if(date!=null){
            lastMod = date;
        }
    }

    @Override
    public void startTransaction() {

    }

    @Override
    public void endTransaction() {

    }

    @Override
    public void cancelTransaction() {

    }

    @Override
    public List<Object> getNoSynchronized() {
        List<Object> list = new ArrayList<Object>();
        for (Product p :products){
            if(!p.isSync()){
                list.add(p);
            }
        }
        return list;
    }

    @Override
    public boolean save(Object entity) {
        if(entity instanceof Product){
            Product p = (Product) entity;
            int i = products.indexOf(p);
            if(i<0){
                products.add(p);
            }
            Date date;
            p.setId(UUID.randomUUID());
            p.setLastMod(date  = new Date());
            lastMod = date;
        }


        return true;
    }

    @Override
    public boolean saveAll(Object... entity) {
        return true;
    }

    @Override
    public void saveLastModification(Date date) {
        lastMod = date;
    }

    @Override
    public Date readLastModification() {
        return lastMod;
    }

    @Override
    public Date readLastSynchronization() {
        return lastSync;
    }

    @Override
    public void saveLastSynchronization(Date lastSynchronization) {
        lastSync = lastSynchronization;
    }
}
