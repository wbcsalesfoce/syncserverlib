package com.sync.test.scenario01;

import com.chainofresponsibility.injection.Link;

/**
 * Created by rramirezb on 14/01/2015.
 */
//@ChainOfResponsibility
public interface AnotherChain extends Link {


    public void holaMundo();
}
