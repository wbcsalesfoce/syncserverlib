package com.sync.test.scenario01;

import java.util.Date;
import java.util.UUID;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class Product {
    private UUID id;
    private String name;
    private Date lastMod;
    private Date lastSync;
    private boolean sync;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getLastMod() {
        return lastMod;
    }

    public void setLastMod(Date lastMod) {
        this.lastMod = lastMod;
    }

    public Date getLastSync() {
        return lastSync;
    }

    public void setLastSync(Date lastSync) {
        this.lastSync = lastSync;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    @Override
    public String toString() {
        return name;
    }
}
