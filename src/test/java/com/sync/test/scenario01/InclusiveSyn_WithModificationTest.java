package com.sync.test.scenario01;

import com.asynchronization.core.DataSyncManager;
import com.asynchronization.core.Main;
import com.asynchronization.core.Store;
import com.asynchronization.core.SyncTool;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.*;

/**
 * Created by rramirezb on 13/01/2015.
 */

/**
 * Test of two inclusive syncs with modifications. the sequence is:
 * first sync starts<b/>
 * one data modified<br/>
 * second sync starts<b/>
 * second sync ends <b/>
 * first sync ends<b/>
 */
public class InclusiveSyn_WithModificationTest {
    @Test
    public void test() {
        final Persistence_ persistence = new Persistence_();


        Store store = new Store(persistence);
        store.getStoreTool().addLink(new ProductStore());


        SyncTool.addTransform(new ProductSyncTransform());
        final DataSyncManager data = new DataSyncManager(store);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        Product p = new Product();
        p.setId(UUID.randomUUID());
        p.setName("Coca.Cola");
        persistence.save(p);

        final Product p2 = new Product();
        p2.setId(UUID.randomUUID());
        p2.setName("Sprite");

        persistence.save(p2);

        final Product p3 = new Product();
        p3.setId(UUID.randomUUID());
        p3.setName("Fanta");

        persistence.save(p3);

        Date lastSync = data.readLastSynchronizationDate();
        Date lastMod = data.readLastModificationDate();
        System.out.println("lastSync " + lastSync);
        System.out.println("lastMod " + lastMod);
        final SyncListenerResults resultOfFirst = new SyncListenerResults();
        final SyncListenerResults resultOfSecond = new SyncListenerResults();
        Callable<Integer> runn1 = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                int s = data.readNoSync().size();
                System.out.println("First sync");
                Main.sync(data, new ServerTest(1000), resultOfFirst);
                return s;
            }
        };

        Callable<Integer> runn2 = new Callable<Integer>() {
            @Override
            public Integer call() {
                System.out.println("Second sync");
                int s = data.readNoSync().size();
                Main.sync(data, new ServerTest(200), resultOfSecond);
                return s;
            }
        };

        Callable<Void> runn3 = new Callable<Void>() {
            @Override
            public Void call() {
//                try {
//                    Thread.sleep(300);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                p2.setName("SPRITE");
                persistence.save(p2);
                System.out.printf("%s modified, %s.\n", p2, p2.getLastMod());
                return null;
            }
        };


        FutureTask<Integer> task1 = new FutureTask<Integer>(runn1);
        FutureTask<Integer> task2 = new FutureTask<Integer>(runn2);
        FutureTask<Void> task3 = new FutureTask<Void>(runn3);

        ExecutorService service = Executors.newFixedThreadPool(3);

        service.execute(task1);
        try {
            Thread.sleep(30);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        service.execute(task3);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        service.execute(task2);

        try {
            Integer s1 = task1.get();
            Integer s2 = task2.get();

            task3.get();
            Assert.assertEquals("F", 3, resultOfFirst.getDataToSend());
            Assert.assertEquals("F", 3, resultOfFirst.getBeforeDiscard());
            Assert.assertEquals("F", 2, resultOfFirst.getAfterDiscard());
            Assert.assertEquals("F", 2, resultOfFirst.getSyncData());

            Assert.assertEquals("S data to send", 1, resultOfSecond.getDataToSend());
            Assert.assertEquals("S before discard", 1, resultOfSecond.getBeforeDiscard());
            Assert.assertEquals("S after discard", 1, resultOfSecond.getAfterDiscard());
            Assert.assertEquals("S sync data", 1, resultOfSecond.getSyncData());

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
