package com.sync.test.scenario01;

import com.asynchronization.core.DataSync;
import com.asynchronization.core.SyncState;

import java.util.Date;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class ProductSync implements DataSync {
    private final Date lastSync;
    private final Date lastMod;
    Product product;


    public ProductSync(Product product) {
        this.product = product;
        lastSync = product.getLastSync();
        lastMod = product.getLastMod();
    }

    @Override
    public <D> D getData() {
        return (D) product;
    }

    @Override
    public Object getId() {
        return product.getId();
    }

    @Override
    public Date getLastModificationDate() {
        return lastMod;
    }

    @Override
    public SyncState getState() {
        return product.isSync()?SyncState.SYNC:SyncState.NO_SYNC;
    }

    @Override
    public Date getSyncDate() {
        return lastSync;
    }

    @Override
    public void setState(SyncState state) {

                product.setSync(SyncState.SYNC.equals(state));

    }

    @Override
    public void setSyncDate(Date date) {
        product.setLastSync(date);
    }

    @Override
    public String toString() {
        return product.toString();
    }
}
