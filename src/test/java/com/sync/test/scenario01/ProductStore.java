package com.sync.test.scenario01;

import com.asynchronization.core.SyncState;
import com.asynchronization.core.ElementStore;

import java.util.Date;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class ProductStore implements ElementStore<Product> {
    @Override
    public Date getLastModification(Product ob) {
        Date date = null;
        if(ob!=null ){
            date = ob.getLastMod();
        }
        return date;
    }

    @Override
    public boolean attend(Object ob) {
        return ob instanceof  Product;
    }

    @Override
    public void setLastModification(Product ob, Date date) {
        if(ob!=null ){
            System.out.printf("Last modification : %s\n", date);
            ob.setLastMod(date);
        }
    }

    @Override
    public SyncState getSyncState(Product ob) {
        SyncState state = SyncState.NO_SYNC;
        if(ob!=null && ob.isSync()){
            state = SyncState.SYNC;
        }
        return state;
    }
}
