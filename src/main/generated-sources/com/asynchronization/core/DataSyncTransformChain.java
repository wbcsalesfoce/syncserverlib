
package com.asynchronization.core;

import com.chainofresponsibility.injection.Chain;

public class DataSyncTransformChain
    extends Chain<com.asynchronization.core.DataSyncTransform>
    implements com.asynchronization.core.DataSyncTransform
{


    public DataSync getDataSync(Object o) {
        com.asynchronization.core.DataSyncTransform link;
        DataSync result = null;
        link = getLink(o, this.links);
        if (link!= null) {
            result = link.getDataSync(o);
        }
        return result;
    }

    public boolean attend(Object ob) {
        return (true);
    }

}
