
package com.asynchronization.core;

import java.util.Date;
import com.chainofresponsibility.injection.Chain;

public class ElementStoreChain
    extends Chain<com.asynchronization.core.ElementStore>
    implements com.asynchronization.core.ElementStore
{


    public Date getLastModification(Object ob) {
        com.asynchronization.core.ElementStore link;
        Date result = null;
        link = getLink(ob, this.links);
        if (link!= null) {
            result = link.getLastModification(ob);
        }
        return result;
    }

    public SyncState getSyncState(Object ob) {
        com.asynchronization.core.ElementStore link;
        SyncState result = null;
        link = getLink(ob, this.links);
        if (link!= null) {
            result = link.getSyncState(ob);
        }
        return result;
    }

    public void setLastModification(Object ob, Date date) {
        com.asynchronization.core.ElementStore link;
        link = getLink(ob, this.links);
        if (link!= null) {
            link.setLastModification(ob, date);
        }
    }

    public boolean attend(Object ob) {
        return (true);
    }

}
