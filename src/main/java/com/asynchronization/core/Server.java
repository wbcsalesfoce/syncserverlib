package com.asynchronization.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * Created by rramirezb on 13/01/2015.
 */
public abstract class Server {

    UUID id;

    public Server() {

        id = UUID.randomUUID();
    }

    protected ServerMessage sendDataSync(UUID id, List<DataSync> datas) {

        List<Object> objects = new ArrayList<Object>();
        Iterator<DataSync> it = datas.iterator();
        while (it.hasNext()) {
            DataSync next = it.next();
            objects.add(next.getData());
        }
        return sendData(id, objects);
    }

    public abstract ServerMessage sendData(UUID id, List<Object> data);
}
