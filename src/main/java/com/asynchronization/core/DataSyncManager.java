package com.asynchronization.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class DataSyncManager {
    Store store;

    public DataSyncManager(Store store) {
        this.store = store;
    }

    public List<DataSync> readNoSync(){
        List<Object> obs = store.readNoSync();

        List<DataSync> syncPack = SyncTool.getPackage(obs);
        return syncPack;
    }
    public void saveSync(List<DataSync> datas){
        List<Object> entities = new ArrayList<Object>();
        for (int i = 0; i < datas.size(); i++) {
            DataSync o = datas.get(i);
              entities.add(o.getData());
        }
        store.store(entities);


    }

    protected void save(Object ob){
        store.store(ob);
    }

    public Date readLastModificationDate(){
        return store.getLastModification();
    }

    public Date readLastSynchronizationDate(){
        return store.getLastSynchronization();
    }
    public void saveLastSynchronizationDate(Date date){
        store.setLastSynchronization(date);
    }

    public Date readLastModification(Object entity) {
        return store.getLastMod(entity);
    }
}
