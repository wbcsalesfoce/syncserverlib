package com.asynchronization.core;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class Sync {
    private final DataSyncManager data;
    private final Server server;
    private SyncListener listener;
    private Date lastModificationDate;
    private UUID id;
    private String tagId;
    private Date synchronizationDate;




    public Sync(DataSyncManager data, Server server, SyncListener listener) {
        this.listener = listener;
        this.data = data;
        this.server = server;
        this.lastModificationDate = data.readLastModificationDate();
        synchronizationDate = data.readLastSynchronizationDate();
        init();
    }

    private void init(){
        if(listener==null) {
            listener = new SyncListener() {

                @Override
                public void message(UUID id, Date date, String message) {

                }

                @Override
                public void dataToSendSize(UUID id, int size) {

                }

                @Override
                public void startingSendingDataToServer(UUID id) {

                }

                @Override
                public void serverSyncFinalized(UUID id, ServerMessage serverMessage) {

                }

                @Override
                public void modifiedDataDiscarded(UUID id, int beforeDiscard, int afterDiscard) {

                }

                @Override
                public void synchronizedData(UUID id, Date sync, List<DataSync> datas) {

                }
            };
        }
    }

    /**
     * Read last modification and compare with las synchronization.
     */
    public boolean existsDataToSend() {
        Date lastMod = data.readLastModificationDate();
        Date lastSync = data.readLastSynchronizationDate();

        return lastSync.before(lastMod);

    }


    public UUID getId() {
        return id;
    }

    public void createId() {

        id = SyncCache.getInstance().createCacheId();
        listener.message(id, new Date(), String.format("Creating sync process %s",id));
    }



    public void storeDataToSynchronize() {
        List<DataSync> datas = data.readNoSync();
        SyncCache.getInstance().store(id, datas);
    }

    public ServerMessage sendData() {
        ServerMessage msg = null;
        if(existsDataToSend()) {
            createId();
            storeDataToSynchronize();
            List<DataSync> datas = SyncCache.getInstance().getData(id);
            listener.message(id, new Date(), String.format("Data to send: %s", datas));
            listener.dataToSendSize(id, datas.size());
            if (!datas.isEmpty()) {
                listener.startingSendingDataToServer(id);
                msg = server.sendDataSync(id, datas);
                listener.serverSyncFinalized(id, msg);
                if(SendState.DATA_SENT.equals( msg.getState())) {
                    synchronizationDate = new Date();
                    int beforeDiscard = datas.size();
                    discardModifiedData();
                    int afterDiscard = SyncCache.getInstance().getData(id).size();
                    listener.modifiedDataDiscarded(id, beforeDiscard, afterDiscard);
                    markSynchronizedData();
                    datas = SyncCache.getInstance().getData(id);
                    listener.synchronizedData(id, synchronizationDate, datas);
                }else{
                    listener.message(id, new Date(), String.format("[%s] %s", msg.getState(), msg.getMessage()));
                }
            }
        }
		clearCache();
        if(msg==null){
            msg = new ServerMessage(id,false,SendState.NO_DATA_TO_SEND, "No data to send", null);
            listener.message(id, new Date(), String.format("[%s] %s", msg.getState(), msg.getMessage()));
        }
        return msg;
    }


    public void discardModifiedData() {
        int removed = 0;
        if (data.readLastModificationDate().after(lastModificationDate)) {
            List<DataSync> datas = SyncCache.getInstance().getData(id);
            Iterator<DataSync> it = datas.iterator();

            while (it.hasNext()) {
                DataSync dataSync = it.next();
                Date lastMod = data.readLastModification(dataSync.getData());
                if (lastMod.after(dataSync.getLastModificationDate())) {
                    it.remove();
                    removed++;
                }
            }

        }



    }

    public void markSynchronizedData() {
        List<DataSync> datas = SyncCache.getInstance().getData(id);

        Iterator<DataSync> it = datas.iterator();

        while (it.hasNext()) {
            DataSync next = it.next();
            next.setSyncDate(synchronizationDate);
            next.setState(SyncState.SYNC);
        }


        data.saveSync(datas);
        synchronizationDate = new Date();
        data.saveLastSynchronizationDate(synchronizationDate);
		
    }

    public void clearCache() {
        SyncCache.getInstance().deleteCache(id);
    }


}
