package com.asynchronization.core;

/**
 * Created by rramirezb on 13/01/2015.
 */
public enum SyncState {
    SYNC, NO_SYNC
}
