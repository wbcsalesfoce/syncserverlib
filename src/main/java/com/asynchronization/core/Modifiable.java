package com.asynchronization.core;

import java.util.Date;

/**
 * Created by rramirezb on 13/01/2015.
 */
public interface Modifiable {
    public Date getLastModificationDate();
    public void setLastModificationDate(Date date);
}
