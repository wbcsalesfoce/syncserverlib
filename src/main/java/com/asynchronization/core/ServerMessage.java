package com.asynchronization.core;

import java.util.UUID;

/**
 * Created by rramirezb on 19/01/2015.
 */
public class ServerMessage {
    private final UUID processId;
    private final boolean success;
    private final SendState state;
    private final String message;
    private final Object rawData;

    public ServerMessage(UUID processId, boolean success, SendState state, String message, Object rawData) {
        this.processId = processId;
        this.success = success;
        this.message = message;
        this.rawData = rawData;
        this.state = state;
    }

    public UUID getProcessId() {
        return processId;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public Object getRawData() {
        return rawData;
    }

    public SendState getState() {
        return state;
    }
}
