package com.asynchronization.core.config;

import com.chainofresponsibility.injection.ChainOfResponsibility;
import com.asynchronization.core.ElementStore;
import com.asynchronization.core.DataSyncTransform;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class CoRConfig {
    @ChainOfResponsibility
    private ElementStore store;

    @ChainOfResponsibility
    private DataSyncTransform sync;
}
