package com.asynchronization.core;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by rramirezb on 19/01/2015.
 */
public interface SyncListener {
    public void message(UUID id, Date date, String message);
    public void dataToSendSize(UUID id, int size);

    void startingSendingDataToServer(UUID id);

    void serverSyncFinalized(UUID id, ServerMessage serverMessage);

    void modifiedDataDiscarded(UUID id, int beforeDiscard, int afterDiscard);

    void synchronizedData(UUID id, Date sync, List<DataSync> datas);
}
