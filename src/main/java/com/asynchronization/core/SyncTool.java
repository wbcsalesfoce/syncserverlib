package com.asynchronization.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class SyncTool {

    static DataSyncTransformChain transforms = new DataSyncTransformChain();

    public static void addTransform(DataSyncTransform<?> transform){
        transforms.addLink(transform);
    }
    public static List<DataSync> getPackage(List<Object> obs) {
        Iterator<Object> it = obs.iterator();
        List<DataSync> list= new ArrayList<DataSync>();
        while (it.hasNext()) {
            Object ob = it.next();
            DataSync ds = transforms.getDataSync(ob);
            list.add(ds);
        }
        return list;
    }
}
