package com.asynchronization.core;

/**
 * Created by rramirezb on 19/01/2015.
 */
public enum SendState {
    NO_DATA_TO_SEND,
    DATA_SENT,
    DATA_NO_SENT
}
