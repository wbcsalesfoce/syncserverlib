package com.asynchronization.core;

import com.chainofresponsibility.injection.Link;

import java.util.Date;

/**
 * Created by rramirezb on 13/01/2015.
 */


public interface ElementStore<D> extends Link {
    public Date getLastModification(D ob);
    public SyncState getSyncState(D ob);

    public void setLastModification(D ob, Date date);
}
