package com.asynchronization.core;

import java.util.Date;

/**
 * Created by rramirezb on 13/01/2015.
 */
public interface Synchronizable {

    public Object getId();

    public Date getLastModificationDate();
    public SyncState getState();
    public Date getSyncDate();

    public void setState(SyncState state);
    public void setSyncDate(Date date);

}
