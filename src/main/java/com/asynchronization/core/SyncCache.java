package com.asynchronization.core;

import java.util.*;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class SyncCache {
    private static SyncCache instance;

    public static SyncCache getInstance() {
        if (instance == null) {
            instance = new SyncCache();
        }
        return instance;
    }

    private SyncCache() {
    }

    private Map<UUID, List<DataSync>> cache;

    {
        cache = new HashMap<UUID, List<DataSync>>();
    }

    public UUID createCacheId() {
        return UUID.randomUUID();
    }

    public synchronized int deleteDataInCache(List<DataSync> data) {
        int deleted = 0;
        Collection<List<DataSync>> values = cache.values();
        for (Iterator<List<DataSync>> iterator = values.iterator(); iterator.hasNext(); ) {
            List<DataSync> next = iterator.next();
            System.out.printf("before delete in cache: \n%s\n%s\n", data, next);
            deleteFromAContainedInB(data, next);
            System.out.printf("after delete in cache: \n%s\n%s\n", data, next);

        }
        return deleted;
    }

    public synchronized static void deleteFromAContainedInB(List<DataSync> a, List<DataSync> b){
        Iterator<DataSync> ai = a.iterator();

        List<DataSync> copyB = new ArrayList<DataSync>(b);

        while (ai.hasNext()) {
            DataSync objectA = ai.next();
            Iterator<DataSync> bi = copyB.iterator();
            while (bi.hasNext()) {
                DataSync objectB = bi.next();
                if(objectA.getId().equals(objectB.getId())&&
                        objectA.getLastModificationDate().equals(objectB.getLastModificationDate())){
                    ai.remove();
                }
            }
        }
    }

    public synchronized void store(UUID id, List<DataSync> data) {
        deleteDataInCache(data);
        cache.put(id, data);
    }

    public synchronized boolean deleteCache(UUID id) {
        return cache.remove(id) != null;
    }

    public synchronized List<DataSync> getData(UUID id) {
        return cache.get(id);
    }
}
