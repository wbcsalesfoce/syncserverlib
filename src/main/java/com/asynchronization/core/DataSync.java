package com.asynchronization.core;

/**
 * Created by rramirezb on 13/01/2015.
 */
public interface DataSync extends Synchronizable{
    public <D> D getData();
}
