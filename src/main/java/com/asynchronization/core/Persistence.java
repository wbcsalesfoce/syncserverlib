package com.asynchronization.core;

import java.util.Date;
import java.util.List;

/**
 * Created by rramirezb on 16/01/2015.
 */
public interface Persistence {

    public void startTransaction();
    public void endTransaction();
    public void cancelTransaction();

    public List<Object> getNoSynchronized();

    public boolean save(Object entity);
    public boolean saveAll(Object... entity);

    public void saveLastModification(Date date);

    public Date readLastModification();

    public Date readLastSynchronization();

    public void saveLastSynchronization(Date lastSynchronization);
}
