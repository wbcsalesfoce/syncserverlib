package com.asynchronization.core;

import com.chainofresponsibility.injection.Link;

/**
 * Created by rramirezb on 13/01/2015.
 */
public interface DataSyncTransform<D> extends Link {
    public DataSync getDataSync(D o );
}
