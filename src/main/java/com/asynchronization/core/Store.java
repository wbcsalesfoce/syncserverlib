package com.asynchronization.core;

import java.util.*;

/**
 * Created by rramirezb on 16/01/2015.
 */
public final class Store {
//    private  Date lastModification;
//    private  Date lastSynchronization;
//    private  Set<Object> data;
    private Persistence persistence;
    private  ElementStoreChain storeTool;

     {
        storeTool = new ElementStoreChain();

    }

    public Store(Persistence persistence) {
        this.persistence = persistence;
    }

    public  ElementStoreChain getStoreTool() {
        return storeTool;
    }


    public synchronized  Date getLastModification(){
        return persistence.readLastModification();
    }


    public synchronized void setLastModification(Date lastModification){
        persistence.saveLastModification(lastModification);
    }


    public synchronized Date getLastSynchronization(){
        return persistence.readLastSynchronization();
    }


    public synchronized void setLastSynchronization(Date lastSynchronization) {
        persistence.saveLastSynchronization(lastSynchronization);
    }

    public Date getLastMod(Object entity) {
        return storeTool.getLastModification(entity);
    }

    public List<Object> readNoSync(){
        List<Object> list = persistence.getNoSynchronized();

        return list;
    }

    public boolean store(Object entity){
        return store(entity, new Date());
    }
    public  boolean store(Object entity, Date date) {



        storeTool.setLastModification(entity, date);
        boolean result = persistence.save(entity);
        persistence.saveLastModification(date);

        return result;

    }

    public int store(List<Object> entities) {
        int result = 0;
        Date date = new Date();
        persistence.startTransaction();
        for (int i = 0; i < entities.size(); i++) {
            Object o = entities.get(i);
            if(store(o, date)){
                result ++;
            }
        }
        if(result>0) {
            persistence.saveLastModification( date);
        }
        persistence.endTransaction();
        return result;
    }

    public void init() {

    }
}
